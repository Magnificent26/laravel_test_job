<?php

namespace App\Console\Commands\Transactions;

use App\Models\Transaction\Handler as TransactionsHandler;
use App\Models\Transaction\Type\Transaction;
use App\Traits\Csv;
use Illuminate\Console\Command;
use Symfony\Component\Console\Command\Command as CommandAlias;
use Throwable;

class Calculate extends Command
{
    const STATUS_CODE_FILE_NOT_FOUND = 3;
    use Csv;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transactions:calculate';

    protected $description = 'The command parses the CSV file and calculates the commission of the operations given in the file';

    private array $fields = [
        Transaction::FIELD_DATE,
        Transaction::FIELD_ID,
        Transaction::FIELD_CLIENT_TYPE,
        Transaction::FIELD_TRANSACTION_TYPE,
        Transaction::FIELD_AMOUNT,
        Transaction::FIELD_CURRENCY
    ];

    public function handle(): int
    {
        $path = $this->ask(
            'Specify the address to the csv file:',
            './input.csv'
        );

        if (!$transactionsData = $this->getCsvData($path)) {
            return self::STATUS_CODE_FILE_NOT_FOUND;
        }

        $handler = new TransactionsHandler($transactionsData);
        if (!$handler->validate()) {
            $this->error('Invalid structure of file');
            return CommandAlias::INVALID;
        }

        if (!$this->handleTransactions($handler)) {
            return CommandAlias::FAILURE;
        }

        foreach ($handler->getTransactions() as $transaction) {
            $this->line($transaction->getCommissionFee());
        }
        return CommandAlias::SUCCESS;
    }

    private function getCsvData($path): ?array
    {
        try {
            return $this->readCsv($path, $this->fields);
        } catch (Throwable $exception) {
            $this->error($exception->getMessage());
            return null;
        }
    }

    private function handleTransactions(TransactionsHandler $handler): bool
    {
        try {
            $handler->execute();
            return true;
        } catch (Throwable $exception) {
            $this->error($exception->getMessage());
            return false;
        }
    }
}
