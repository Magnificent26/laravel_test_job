<?php
namespace App\Models\Client;

use App\Models\Transaction\Type\Deposit;
use App\Models\Transaction\Type\Transaction;
use Throwable;

abstract class Client
{
    private int $id;
    private array $transactions = [];
    protected float $depositCharge = 0.0003;
    protected float $withdrawCharge = 0.005;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function addTransaction(Transaction $transaction): void
    {
        $this->transactions[] = $transaction;
        if ($transaction instanceof Deposit) {
            $this->deposit($transaction);
        } else {
            $this->withdraw($transaction);
        }
    }

    protected function deposit(Transaction $transaction): void
    {
        $commission = $transaction->getAmount() * $this->depositCharge;
        $transaction->setCommission($commission);
    }

    protected function withdraw(Transaction $transaction): void
    {
        $commission = $transaction->getAmount() * $this->withdrawCharge;
        $transaction->setCommission($commission);
    }

    public static function getClient(int $id, string $type) : Client
    {
        $clientHook = 'client_' . $id . '_' . $type;

        try {
            return app($clientHook);
        } catch (Throwable) {
            app()->singleton($clientHook, static function () use ($id, $type) {
                return static::getClientType($id, $type);
            });
            return app($clientHook);
        }
    }

    public static function getClientType(int $id, string $name): self
    {
        return match ($name) {
            'business' => new TypeBusiness($id),
            default => new TypePrivate($id),
        };
    }

    /**
     * @return array|Transaction[]
     */
    public function getTransactions(): array
    {
        return $this->transactions;
    }
}
