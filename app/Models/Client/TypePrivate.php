<?php
namespace App\Models\Client;

use App\Models\Transaction\Currency\Currency;
use App\Models\Transaction\Type\Transaction;
use Exception;

class TypePrivate extends Client
{
    const WEEKLY_FREE_WITHDRAW_AMOUNT_LIMIT = 1000;
    const WEEKLY_FREE_WITHDRAW_COUNT_LIMIT = 3;

    public array $limits = [];

    public function __construct(int $id)
    {
        parent::__construct($id);
        $this->withdrawCharge = 0.003;
    }

    /**
     * @throws Exception
     */
    protected function withdraw(Transaction $transaction): void
    {
        $amount = $transaction->getAmount();
        if (!$this->hasFreeTransactions($transaction)) {
            $amount = $this->applyFreeTransactionCondition($transaction);
        }

        $commission = $amount * $this->withdrawCharge;
        $transaction->setCommission($commission);
    }

    /**
     * @throws Exception
     */
    private function applyFreeTransactionCondition(Transaction $transaction): float
    {
        $currency = new Currency();
        $amount = $transaction->getAmount();
        $availableFreeCharge = $this->getFreeWithdraw($transaction->getWeek());
        $converted = $currency->convertToMain($amount, $transaction->getCurrencyCode());
        $converted = $converted - $availableFreeCharge;

        if ($converted < 0) {
            $this->setFreeWithdraw(abs($converted), $transaction->getWeek());
            $converted = 0;
        } else {
            $this->setFreeWithdraw(0, $transaction->getWeek());
        }
        return $currency->convertFromMain($converted, $transaction->getCurrencyCode());
    }

    private function hasFreeTransactions(Transaction $transaction): bool
    {
        return $this->getWeekTransactions($transaction->getWeek()) <= static::WEEKLY_FREE_WITHDRAW_COUNT_LIMIT;
    }

    private function getFreeWithdraw(int $week): float
    {
        if (!array_key_exists($week, $this->limits)) {
            $this->setFreeWithdraw(self::WEEKLY_FREE_WITHDRAW_AMOUNT_LIMIT, $week);
        }
        return $this->limits[$week];
    }

    private function setFreeWithdraw(float $amount, int $week)
    {
        $this->limits[$week] = $amount;
    }

    private function getWeekTransactions(int $week): array
    {
        $result = [];
        foreach ($this->getTransactions() as $transaction) {
            if ($transaction->getWeek() == $week) {
                $result[] = $transaction;
            }
        }
        return $result;
    }
}
