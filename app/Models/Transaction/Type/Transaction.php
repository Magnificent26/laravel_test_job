<?php
namespace App\Models\Transaction\Type;

use App\Models\Client\Client;
use App\Models\Transaction\Currency\Currency;
use DateTime;
use Exception;
use Throwable;

abstract class Transaction
{
    const FIELD_ID = 'id';
    const FIELD_DATE = 'date';
    const FIELD_CLIENT_TYPE = 'client-type';
    const FIELD_TRANSACTION_TYPE = 'transaction-type';
    const FIELD_AMOUNT = 'amount';
    const FIELD_CURRENCY = 'currency';

    protected Client $client;
    private DateTime $date;
    private float $amount;
    private float $commission;
    private string $currency;

    public function execute(): bool
    {
        $this->client->addTransaction($this);
        return true;
    }

    public function setClient(Client $client): static
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @throws Exception
     */
    public function setDate(string $date): static
    {
        try {
            $this->date = new DateTime($date);
        } catch (Throwable) {
            throw new Exception('Invalid date format');
        }
        return $this;
    }

    public function setAmount(float $amount): static
    {
        $this->amount = $amount;
        return $this;
    }

    public function setCommission(float $commission): void
    {
        $this->commission = $commission;
    }

    public function setCurrencyCode(string $currencyCode): static
    {
        $this->currency = $currencyCode;
        return $this;
    }

    public function getCurrencyCode(): string
    {
        return $this->currency;
    }

    public function getWeek(): int
    {
        $date = $this->date->format("Y-m-d");
        $weekSeconds = (60 * 60 * 24 * 7);
        return ((time() - strtotime('1970-09-01')) - (time() - strtotime($date))) / $weekSeconds;
    }

    public function getCommissionFee(): string
    {
        $currency = new Currency();
        return $currency->round($this->commission, $this->getCurrencyCode());
    }

    public function getAmount(): float
    {
        return $this->amount;
    }
}
