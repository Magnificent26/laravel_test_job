<?php
namespace App\Models\Transaction;

use App\Models\Client\Client;
use App\Models\Transaction\Type\Transaction;
use Exception;
use Illuminate\Support\Facades\Validator;

class Handler
{
    private array $transactionsData;

    private array $transactions = [];

    public function __construct(array $transactions)
    {
        $this->transactionsData = $transactions;
    }

    /**
     * @throws Exception
     */
    public function execute(): void
    {
        foreach ($this->transactionsData as $data) {
            $client = Client::getClient(
                $data[Transaction::FIELD_ID],
                $data[Transaction::FIELD_CLIENT_TYPE]
            );
            $transaction = TransactionBuilder::create(
                $client,
                $data[Transaction::FIELD_DATE],
                $data[Transaction::FIELD_AMOUNT],
                $data[Transaction::FIELD_CURRENCY],
                $data[Transaction::FIELD_TRANSACTION_TYPE]
            );
            $this->transactions[] = $transaction;
            $transaction->execute();
        }
    }


    public function validate(): bool
    {
        $rules = [
            '*.' . Transaction::FIELD_DATE => 'required|date',
            '*.' . Transaction::FIELD_ID => 'required|int',
            '*.' . Transaction::FIELD_CLIENT_TYPE => 'required|in:private,business',
            '*.' . Transaction::FIELD_TRANSACTION_TYPE => 'required|in:deposit,withdraw',
            '*.' . Transaction::FIELD_AMOUNT => 'required|numeric',
            '*.' . Transaction::FIELD_CURRENCY => 'required|string'
        ];

        $validator = Validator::make(
            $this->transactionsData,
            $rules
        );
        return $validator->passes();
    }

    /**
     * @return array|Transaction[]
     */
    public function getTransactions(): array
    {
        return $this->transactions;
    }
}
