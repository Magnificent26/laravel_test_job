<?php
namespace App\Models\Transaction\Currency;

use Exception;
use Throwable;

class Currency
{
    const API_URL = 'https://developers.paysera.com/tasks/api/currency-exchange-rates';

    /**
     * @throws Exception
     */
    public function convertToMain(float $amount, string $currencyCode): float
    {
        if ($this->getMainCurrency() == $currencyCode) {
            return $amount;
        }

        $rate = $this->getRate($currencyCode);
        return $amount / $rate;
    }

    /**
     * @throws Exception
     */
    public function convertFromMain(float $amount, string $currencyCode): float
    {
        if ($this->getMainCurrency() == $currencyCode) {
            return $amount;
        }

        $rate = $this->getRate($currencyCode);
        return $amount * $rate;
    }

    /**
     * @throws Exception
     */
    private function getRate(string $currencyCode): float
    {
        $currencies = $this->loadCurrencies();
        try {
            return (float) $currencies['rates'][$currencyCode];
        } catch (Throwable $exception) {
            throw new Exception('Currency "' . $currencyCode . '" not found.');
        }
    }

    /**
     * @throws Exception
     */
    public function getMainCurrency(): string
    {
        $currencies = $this->loadCurrencies();
        try {
            return $currencies['base'];
        } catch (Throwable $exception) {
            throw new Exception('Can\t detect main currency.');
        }

    }

    /**
     * @throws Exception
     */
    private function loadCurrencies(): array
    {
        $hook = 'api_currencies';
        try {
            return app($hook);
        } catch (Throwable) {
            app()->singleton($hook, static function () {
                try {
                    $response = file_get_contents(self::API_URL);
                    return json_decode($response, true);
                } catch (Throwable) {
                    throw new Exception('Failed to load currencies');
                }
            });
            return app($hook);
        }
    }

    public function round(float $amount, string $currency): string
    {
        return match ($currency) {
            'JPY' => number_format($amount, 0, '.', ''),
            default => number_format($amount, 2, '.', ''),
        };
    }
}
