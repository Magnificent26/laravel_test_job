<?php
namespace App\Models\Transaction;

use App\Models\Client\Client;
use App\Models\Transaction\Type\Deposit;
use App\Models\Transaction\Type\Transaction;
use App\Models\Transaction\Type\Withdraw;
use Exception;

class TransactionBuilder
{
    /**
     * @throws Exception
     */
    public static function create(
        Client $client,
        string $date,
        float $amount,
        string $currencyCode,
        string $transactionType
    ): Transaction
    {
        $transaction = static::getTypeByName($transactionType);
        $transaction->setClient($client)
            ->setDate($date)
            ->setAmount($amount)
            ->setCurrencyCode($currencyCode);
        return $transaction;
    }

    public static function getTypeByName(string $name): Transaction
    {
        return match ($name) {
            'deposit' => new Deposit(),
            default => new Withdraw(),
        };
    }
}
