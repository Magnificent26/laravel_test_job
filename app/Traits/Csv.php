<?php
namespace App\Traits;

use Exception;
use Throwable;

trait Csv
{
    /**
     *
     * @throws Exception
     */
    public function readCsv(string $path, ?array $fields = null): array|false
    {
        $result = [];

        try {
            if (!$open = fopen($path, "r")) {
                throw new Exception('File not found');
            }
        } catch (Throwable) {
            throw new Exception('File not found');
        }

        while (($data = fgetcsv($open)) !== false) {
            if ($fields) {
                foreach ($fields as $key => $name) {
                    $data[$name] = $data[$key];
                    unset($data[$key]);
                }
            }

            $result[] = $data;
        }

        fclose($open);
        return $result;
    }
}
