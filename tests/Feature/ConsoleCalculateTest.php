<?php

namespace Tests\Feature;

use App\Console\Commands\Transactions\Calculate;
use Symfony\Component\Console\Command\Command as CommandAlias;
use Tests\TestCase;

class ConsoleCalculateTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_valid_file(): void
    {
        $this->artisan('transactions:calculate')
            ->expectsQuestion('Specify the address to the csv file:', './input.csv')
            ->assertExitCode(CommandAlias::SUCCESS);
    }

    public function test_invalid_file(): void
    {
        $this->artisan('transactions:calculate')
            ->expectsQuestion('Specify the address to the csv file:', './input_invalid.csv')
            ->assertExitCode(CommandAlias::INVALID);
    }

    public function test_file_not_found(): void
    {
        $this->artisan('transactions:calculate')
            ->expectsQuestion('Specify the address to the csv file:', './non-existent_file.csv')
            ->assertExitCode(Calculate::STATUS_CODE_FILE_NOT_FOUND);
    }

    public function test_unsupported_currency(): void
    {
        $this->artisan('transactions:calculate')
            ->expectsQuestion('Specify the address to the csv file:', './input_unsupported_currency.csv')
            ->assertExitCode(CommandAlias::FAILURE);
    }
}
